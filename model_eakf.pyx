import numpy as np
from libc.math cimport exp
from libc.stdlib cimport rand, RAND_MAX

cimport cython
cimport numpy as np

ctypedef np.int_t DTYPE_INT
ctypedef np.float_t DTYPE_FLOAT


"""
@cython.cdivision(True)
cpdef int poissonRandom(double expectedValue):
  cdef int k = 0
  cdef double limit = exp(-expectedValue)
  cdef double p = 1
  while (p > limit):
    k += 1
    p *= rand() / (RAND_MAX + 1.0)
  return k - 1
"""


poissonRandom = np.random.poisson


def model_eakf(
    np.ndarray[DTYPE_INT, ndim=1] nl,
    np.ndarray[DTYPE_INT, ndim=1] part,
    np.ndarray[DTYPE_FLOAT, ndim=1] C,
    np.ndarray[DTYPE_FLOAT, ndim=1] Cave,
    np.ndarray[DTYPE_FLOAT, ndim=1] S,
    np.ndarray[DTYPE_FLOAT, ndim=1] E,
    np.ndarray[DTYPE_FLOAT, ndim=1] Ir,
    np.ndarray[DTYPE_FLOAT, ndim=1] Iu,
    np.ndarray[DTYPE_FLOAT, ndim=1] para,
    np.ndarray[DTYPE_INT, ndim=1] betamap,
    np.ndarray[DTYPE_INT, ndim=1] alphamap):

    cdef unsigned int num_mp = nl.shape[0]
    cdef unsigned int num_loc = part.shape[0] - 1

    cdef np.ndarray[DTYPE_FLOAT, ndim=1] newS = np.zeros(num_mp, dtype=np.float64)
    cdef np.ndarray[DTYPE_FLOAT, ndim=1] newE = np.zeros(num_mp, dtype=np.float64)
    cdef np.ndarray[DTYPE_FLOAT, ndim=1] newIr = np.zeros(num_mp, dtype=np.float64)
    cdef np.ndarray[DTYPE_FLOAT, ndim=1] newIu = np.zeros(num_mp, dtype=np.float64)
    cdef np.ndarray[DTYPE_FLOAT, ndim=1] dailyIr = np.zeros(num_mp, dtype=np.float64)
    cdef np.ndarray[DTYPE_FLOAT, ndim=1] dailyIu = np.zeros(num_mp, dtype=np.float64)

    # extracting parameters
    cdef float Z = para[0]
    cdef float D = para[1]
    cdef float mu = para[2]
    cdef float theta = para[3]

    # day and night
    cdef float dt1 = 1.0 / 3
    cdef float dt2 = 1 - dt1

    # creating vectors
    cdef np.ndarray[DTYPE_FLOAT, ndim=1] ND = np.zeros(num_loc)
    cdef np.ndarray[DTYPE_FLOAT, ndim=1] IrD = np.zeros(num_loc)
    cdef np.ndarray[DTYPE_FLOAT, ndim=1] IuD = np.zeros(num_loc)
    cdef np.ndarray[DTYPE_FLOAT, ndim=1] SD = np.zeros(num_loc)
    cdef np.ndarray[DTYPE_FLOAT, ndim=1] ED = np.zeros(num_loc)
    cdef np.ndarray[DTYPE_FLOAT, ndim=1] RD = np.zeros(num_loc)
    cdef np.ndarray[DTYPE_FLOAT, ndim=1] RentD = np.zeros(num_loc)
    cdef np.ndarray[DTYPE_FLOAT, ndim=1] EentD = np.zeros(num_loc)
    cdef np.ndarray[DTYPE_FLOAT, ndim=1] IuentD = np.zeros(num_loc)
    cdef np.ndarray[DTYPE_FLOAT, ndim=1] NN = np.zeros(num_loc)
    cdef np.ndarray[DTYPE_FLOAT, ndim=1] IrN = np.zeros(num_loc)
    cdef np.ndarray[DTYPE_FLOAT, ndim=1] IuN = np.zeros(num_loc)
    cdef np.ndarray[DTYPE_FLOAT, ndim=1] SN = np.zeros(num_loc)
    cdef np.ndarray[DTYPE_FLOAT, ndim=1] EN = np.zeros(num_loc)
    cdef np.ndarray[DTYPE_FLOAT, ndim=1] RN = np.zeros(num_loc)
    cdef np.ndarray[DTYPE_FLOAT, ndim=1] RentN = np.zeros(num_loc)
    cdef np.ndarray[DTYPE_FLOAT, ndim=1] EentN = np.zeros(num_loc)
    cdef np.ndarray[DTYPE_FLOAT, ndim=1] IuentN = np.zeros(num_loc)
    cdef np.ndarray[DTYPE_FLOAT, ndim=1] popleft = np.zeros(num_loc)
    cdef np.ndarray[DTYPE_FLOAT, ndim=1] tempS = np.zeros(num_mp)
    cdef np.ndarray[DTYPE_FLOAT, ndim=1] tempE = np.zeros(num_mp)
    cdef np.ndarray[DTYPE_FLOAT, ndim=1] tempIu = np.zeros(num_mp)
    cdef np.ndarray[DTYPE_FLOAT, ndim=1] tempIr = np.zeros(num_mp)
    cdef np.ndarray[DTYPE_FLOAT, ndim=1] tempR = np.zeros(num_mp)
    cdef np.ndarray[DTYPE_FLOAT, ndim=1] R = np.zeros(num_mp)
    cdef np.ndarray[DTYPE_FLOAT, ndim=1] newR = np.zeros(num_mp)

    # compute pop left
    cdef unsigned int i,j
    for i in range(num_loc):
        for j in range(part[i] + 1, part[i+1]):
            popleft[i] += Cave[j]

    # assign intermediate
    for i in range(num_mp):
        R[i] = max(C[i]-S[i]-E[i]-Ir[i]-Iu[i], 0.0)
        tempS[i] = S[i]
        tempE[i] = E[i]
        tempIr[i] = Ir[i]
        tempIu[i] = Iu[i]
        tempR[i] = R[i]

    # daytime transmission
    # compute ND
    for i in range(num_loc):
        ND[i] = C[part[i]]
        for j in range(part[i] + 1, part[i+1]):
            ND[i] = ND[i]+Ir[j]  # reported infections (no mobility)

    for i in range(num_loc):
        for j in range(part[i]+1, part[i+1]):
            ND[nl[j]] = ND[nl[j]] + C[j] - Ir[j]  # commuting with reported infections removed

    # comput IrD,IuD,SD,ED,RD
    for i in range(num_loc):
        for j in range(part[i], part[i+1]):
            IrD[i] = IrD[i] + Ir[j]
            IuD[nl[j]] = IuD[nl[j]] + Iu[j]
            SD[nl[j]] = SD[nl[j]] + S[j]
            ED[nl[j]] = ED[nl[j]] + E[j]
            RD[nl[j]] = RD[nl[j]] + R[j]

    # compute RentD, EentD and IuentD
    for i in range(num_loc):
        for j in range(part[i] + 1, part[i+1]):
            RentD[nl[j]] = RentD[nl[j]] + Cave[j] * RD[i] / (ND[i] - IrD[i])
            EentD[nl[j]] = EentD[nl[j]] + Cave[j] * ED[i] / (ND[i] - IrD[i])
            IuentD[nl[j]] = IuentD[nl[j]] + Cave[j] * IuD[i] / (ND[i] - IrD[i])

    # compute for each subpopulation
    cdef float beta, alpha, Eexpr, Eexpu, Einfr, Erecr, Erecu, ERenter, ERleft, EEenter, EEleft, EIuenter, EIuleft
    cdef float expr, expu, infr, infu, recr, recu, Rleft, Eleft, Iuleft
    cdef int Renter, Eenter, Iuenter
    for i in range(num_loc):
        for j in range(part[i], part[i+1]):
            beta = para[betamap[nl[j]]]
            alpha = para[alphamap[i]]
            Eexpr = beta * S[j] * IrD[nl[j]] / ND[nl[j]] * dt1  # new exposed due to reported cases
            Eexpu = mu * beta * S[j] * IuD[nl[j]] / ND[nl[j]] * dt1  # new exposed due to unreported cases
            Einfr = alpha * E[j] / Z * dt1  # new reported cases
            Einfu = (1-alpha) * E[j] / Z * dt1  # new unreported cases
            Erecr = Ir[j] / D * dt1  # new recovery of reported cases
            Erecu = Iu[j] /D * dt1  # new recovery of unreported cases

            ERenter = theta * dt1 * (C[j] - Ir[j]) / ND[nl[j]] * RentD[nl[j]]  # incoming R
            ERleft = theta * dt1 * R[j] / (ND[nl[j]] - IrD[nl[j]]) * popleft[nl[j]]  # outgoing R
            EEenter = theta * dt1 * (C[j] - Ir[j]) / ND[nl[j]] * EentD[nl[j]]  # incoming E
            EEleft = theta * dt1 * E[j] / (ND[nl[j]] - IrD[nl[j]]) * popleft[nl[j]]  # outgoing E
            EIuenter = theta * dt1 * (C[j] - Ir[j]) / ND[nl[j]] * IuentD[nl[j]]  # incoming Iu
            EIuleft = theta * dt1 * Iu[j] / (ND[nl[j]] - IrD[nl[j]]) * popleft[nl[j]]  # outgoing Iu

            expr = min(poissonRandom(Eexpr), (S[j]*dt1))
            expu = min(poissonRandom(Eexpu), (S[j]*dt1))
            infr = min(poissonRandom(Einfr), (E[j]*dt1))
            infu = min(poissonRandom(Einfu), (E[j]*dt1))
            recr = min(poissonRandom(Erecr), (Ir[j]*dt1))
            recu = min(poissonRandom(Erecu), (Iu[j]*dt1))

            Renter = poissonRandom(ERenter)
            Rleft = min(poissonRandom(ERleft), (R[j]*dt1))
            Eenter = poissonRandom(EEenter)
            Eleft = min(poissonRandom(EEleft), (E[j]*dt1))
            Iuenter = poissonRandom(EIuenter)
            Iuleft = min(poissonRandom(EIuleft), (Iu[j]*dt1))

            tempR[j] = max(int(tempR[j]+recr+recu+Renter-Rleft), 0)
            tempE[j] = max(int(tempE[j]+expr+expu-infr-infu+Eenter-Eleft), 0)
            tempIr[j] = max(int(tempIr[j]+infr-recr), 0)
            tempIu[j] = max(int(tempIu[j]+infu-recu+Iuenter-Iuleft), 0)
            dailyIr[j] = max(int(dailyIr[j]+infr), 0)
            dailyIu[j] = max((dailyIu[j]+infu), 0)
            tempS[j] = max((C[j]-tempE[j]-tempIr[j]-tempIu[j]-tempR[j]), 0)

    # nighttime transmission
    # assgn new S, E, Iu, Ir, R
    for i in range(num_mp):
        newS[i] = tempS[i]
        newE[i] = tempE[i]
        newIr[i] = tempIr[i]
        newIu[i] = tempIu[i]
        newR[i] = tempR[i]

    # compute NN
    for i in range(num_loc):
        for j in range(part[i], part[i+1]):
            NN[i] = NN[i] + C[j]

    # comput IrN,IuN,SN,EN,RN
    for i in range(num_loc):
        for j in range(part[i], part[i+1]):
            IrN[i] = IrN[i] + tempIr[j]
            IuN[i] = IuN[i] + tempIu[j]
            SN[i] = SN[i] + tempS[j]
            EN[i] = EN[i] + tempE[j]
            RN[i] = RN[i] + tempR[j]

    # compute RentN, EentN and IuentN
    for i in range(num_loc):
        for j in range(part[i]+1, part[i+1]):
            RentN[nl[j]] = RentN[nl[j]] + Cave[j] * RN[i] / (NN[i] - IrN[i])
            EentN[nl[j]] = EentN[nl[j]] + Cave[j] * EN[i] / (NN[i] - IrN[i])
            IuentN[nl[j]] = IuentN[nl[j]] + Cave[j] * IuN[i] / (NN[i] - IrN[i])

    # compute for each subpopulation
    for i in range(num_loc):
        for j in range(part[i], part[i+1]):
            beta = para[betamap[i]]
            alpha = para[alphamap[i]]
            Eexpr = beta * tempS[j] * IrN[i] / NN[i] * dt2  # new exposed due to reported cases
            Eexpu = mu * beta * tempS[j] * IuN[i] / NN[i] * dt2  # new exposed due to unreported cases
            Einfr = alpha * tempE[j] / Z * dt2  # new reported cases
            Einfu = (1-alpha) * tempE[j] / Z * dt2  # new unreported cases
            Erecr = tempIr[j] / D * dt2  # new recovery of reported cases
            Erecu = tempIu[j] / D * dt2  # new recovery of unreported cases

            ERenter = theta * dt2 * C[j] / NN[i] * RentN[i]  # incoming r
            ERleft = theta * dt2 * tempR[j] / (NN[i] - IrN[i]) * popleft[i]  # outgoing r
            EEenter = theta * dt2 * C[j] / NN[i] * EentN[i]  # incoming e
            EEleft = theta * dt2 * tempE[j] / (NN[i] - IrN[i]) * popleft[i]  # outgoing e
            EIuenter = theta * dt2 * C[j] / NN[i] * IuentN[i]  # incoming iu
            EIuleft = theta * dt2 * tempIu[j] / (NN[i] - IrN[i]) * popleft[i]  # outgoing iu

            expr = min(poissonRandom(Eexpr), (tempS[j]*dt2))
            expu = min(poissonRandom(Eexpu), (tempS[j]*dt2))
            infr = min(poissonRandom(Einfr), (tempE[j]*dt2))
            infu = min(poissonRandom(Einfu), (tempE[j]*dt2))
            recr = min(poissonRandom(Erecr), (tempIr[j]*dt2))
            recu = min(poissonRandom(Erecu), (tempIu[j]*dt2))

            Renter = poissonRandom(ERenter)
            Rleft = min(poissonRandom(ERleft), (tempR[j]*dt2))
            Eenter = poissonRandom(EEenter)
            Eleft = min(poissonRandom(EEleft), (tempE[j]*dt2))
            Iuenter = poissonRandom(EIuenter)
            Iuleft = min(poissonRandom(EIuleft), (tempIu[j]*dt2))

            newR[j] = max(int(newR[j] + recr + recu + Renter - Rleft), 0)
            newE[j] = max(int(newE[j] + expr + expu - infr - infu + Eenter - Eleft), 0)
            newIr[j] = max(int(newIr[j] + infr - recr), 0)
            newIu[j] = max(int(newIu[j] + infu - recu + Iuenter - Iuleft), 0)
            dailyIr[j] = max(int(dailyIr[j] + infr), 0)
            dailyIu[j] = max(int(dailyIu[j] + infu), 0)
            newS[j] = max(int(C[j] - newE[j] - newIr[j] - newIu[j] - newR[j]), 0)

    return newS, newE, newIr, newIu, dailyIr, dailyIu
