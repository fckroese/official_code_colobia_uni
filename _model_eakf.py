import copy
import numpy as np


def model_eakf(nl, part, C, Cave, S, E, Ir, Iu, para, betamap, alphamap):
    num_mp = len(nl)
    num_loc = len(part) - 1
    betamap = betamap.astype(int) - 1
    alphamap = alphamap.astype(int) - 1

    newS = make_matrix(num_mp, 1, np.float64)
    newE = make_matrix(num_mp, 1, np.float64)
    newIr = make_matrix(num_mp, 1, np.float64)
    newIu = make_matrix(num_mp, 1, np.float64)
    dailyIr = make_matrix(num_mp, 1, np.float64)
    dailyIu = make_matrix(num_mp, 1, np.float64)

    # extracting parameters
    Z = para[0]
    D = para[1]
    mu = para[2]
    theta = para[3]

    # day and night
    dt1 = 1.0 / 3
    dt2 = 1 - dt1

    # creating vectors
    ND = np.zeros(num_loc)
    IrD = np.zeros(num_loc)
    IuD = np.zeros(num_loc)
    SD = np.zeros(num_loc)
    ED = np.zeros(num_loc)
    RD = np.zeros(num_loc)
    RentD = np.zeros(num_loc)
    EentD = np.zeros(num_loc)
    IuentD = np.zeros(num_loc)
    NN = np.zeros(num_loc)
    IrN = np.zeros(num_loc)
    IuN = np.zeros(num_loc)
    SN = np.zeros(num_loc)
    EN = np.zeros(num_loc)
    RN = np.zeros(num_loc)
    RentN = np.zeros(num_loc)
    EentN = np.zeros(num_loc)
    IuentN = np.zeros(num_loc)
    popleft = np.zeros(num_loc)
    tempS = np.zeros(num_mp)
    tempE = np.zeros(num_mp)
    tempIu = np.zeros(num_mp)
    tempIr = np.zeros(num_mp)
    tempR = np.zeros(num_mp)
    R = np.zeros(num_mp)
    newR = np.zeros(num_mp)

    # compute pop left
    for i in range(num_loc):
        for j in range(part[i] + 1, part[i+1]):
            popleft[i] = popleft[i] + Cave[j]

    # assign intermediate
    for i in range(num_mp):
        R[i] = max(C[i]-S[i]-E[i]-Ir[i]-Iu[i], 0.0)
        tempS[i] = S[i]
        tempE[i] = E[i]
        tempIr[i] = Ir[i]
        tempIu[i] = Iu[i]
        tempR[i] = R[i]

    # daytime transmission
    # compute ND
    for i in range(num_loc):
        ND[i] = C[part[i]]
        for j in range(part[i] + 1, part[i+1]):
            ND[i] = ND[i]+Ir[j]  # reported infections (no mobility)

    for i in range(num_loc):
        for j in range(part[i]+1, part[i+1]):
            ND[nl[j]] = ND[nl[j]] + C[j] - Ir[j]  # commuting with reported infections removed

    # comput IrD,IuD,SD,ED,RD
    for i in range(num_loc):
        for j in range(part[i], part[i+1]):
            IrD[i] = IrD[i] + Ir[j]
            IuD[nl[j]] = IuD[nl[j]] + Iu[j]
            SD[nl[j]] = SD[nl[j]] + S[j]
            ED[nl[j]] = ED[nl[j]] + E[j]
            RD[nl[j]] = RD[nl[j]] + R[j]

    # compute RentD, EentD and IuentD
    for i in range(num_loc):
        for j in range(part[i], part[i+1]):
            RentD[nl[j]] = RentD[nl[j]] + Cave[j] * RD[i] / (ND[i] - IrD[i])
            EentD[nl[j]] = EentD[nl[j]] + Cave[j] * ED[i] / (ND[i] - IrD[i])
            IuentD[nl[j]] = IuentD[nl[j]] + Cave[j] * IuD[i] / (ND[i] - IrD[i])

    # compute for each subpopulation
    for i in range(num_loc):
        for j in range(part[i], part[i+1]):
            beta = para[betamap[nl[j]]]
            alpha = para[alphamap[i]]
            Eexpr = beta * S[j] * IrD[nl[j]] / ND[nl[j]] * dt1  # new exposed due to reported cases
            Eexpu = mu * beta * S[j] * IuD[nl[j]] / ND[nl[j]] * dt1  # new exposed due to unreported cases
            Einfr = alpha * E[j] / Z * dt1  # new reported cases
            Einfu = (1-alpha) * E[j] / Z * dt1  # new unreported cases
            Erecr = Ir[j] / D * dt1  # new recovery of reported cases
            Erecu = Iu[j] / D * dt1  # new recovery of unreported cases

            ERenter = theta * dt1 * (C[j] - Ir[j]) / ND[nl[j]] * RentD[nl[j]]  # incoming R
            ERleft = theta * dt1 * R[j] / (ND[nl[j]] - IrD[nl[j]]) * popleft[nl[j]]  # outgoing R
            EEenter = theta * dt1 * (C[j] - Ir[j]) / ND[nl[j]] * EentD[nl[j]]  # incoming E
            EEleft = theta * dt1 * E[j] / (ND[nl[j]] - IrD[nl[j]]) * popleft[nl[j]]  # outgoing E
            EIuenter = theta * dt1 * (C[j] - Ir[j]) / ND[nl[j]] * IuentD[nl[j]]  # incoming Iu
            EIuleft = theta * dt1 * Iu[j] / (ND[nl[j]] - IrD[nl[j]]) * popleft[nl[j]]  # outgoing Iu

            expr = min(np.random.poisson(Eexpr), (S[j]*dt1))
            expu = min(np.random.poisson(Eexpu), (S[j]*dt1))
            infr = min(np.random.poisson(Einfr), (E[j]*dt1))
            infu = min(np.random.poisson(Einfu), (E[j]*dt1))
            recr = min(np.random.poisson(Erecr), (Ir[j]*dt1))
            recu = min(np.random.poisson(Erecu), (Iu[j]*dt1))

            Renter = np.random.poisson(ERenter)
            Rleft = min(np.random.poisson(ERleft), (R[j]*dt1))
            Eenter = np.random.poisson(EEenter)
            Eleft = min(np.random.poisson(EEleft), (E[j]*dt1))
            Iuenter = np.random.poisson(EIuenter)
            Iuleft = min(np.random.poisson(EIuleft), (Iu[j]*dt1))

            tempR[j] = max((tempR[j]+recr+recu+Renter-Rleft), 0)
            tempE[j] = max((tempE[j]+expr+expu-infr-infu+Eenter-Eleft), 0)
            tempIr[j] = max((tempIr[j]+infr-recr), 0)
            tempIu[j] = max((tempIu[j]+infu-recu+Iuenter-Iuleft), 0)
            dailyIr[j] = max((dailyIr[j]+infr), 0)
            dailyIu[j] = max((dailyIu[j]+infu), 0)
            tempS[j] = max((C[j]-tempE[j]-tempIr[j]-tempIu[j]-tempR[j]), 0)

    # nighttime transmission
    # assgn new S, E, Iu, Ir, R
    for i in range(num_mp):
        newS[i] = tempS[i]
        newE[i] = tempE[i]
        newIr[i] = tempIr[i]
        newIu[i] = tempIu[i]
        newR[i] = tempR[i]

    # compute NN
    for i in range(num_loc):
        for j in range(part[i], part[i+1]):
            NN[i] = NN[i] + C[j]

    # comput IrN,IuN,SN,EN,RN
    for i in range(num_loc):
        for j in range(part[i], part[i+1]):
            IrN[i] = IrN[i] + tempIr[j]
            IuN[i] = IuN[i] + tempIu[j]
            SN[i] = SN[i] + tempS[j]
            EN[i] = EN[i] + tempE[j]
            RN[i] = RN[i] + tempR[j]

    # compute RentN, EentN and IuentN
    for i in range(num_loc):
        for j in range(part[i]+1, part[i+1]):
            RentN[nl[j]] = RentN[nl[j]] + Cave[j] * RN[i] / (NN[i] - IrN[i])
            EentN[nl[j]] = EentN[nl[j]] + Cave[j] * EN[i] / (NN[i] - IrN[i])
            IuentN[nl[j]] = IuentN[nl[j]] + Cave[j] * IuN[i] / (NN[i] - IrN[i])

    # compute for each subpopulation
    for i in range(num_loc):
        for j in range(part[i], part[i+1]):
            beta = para[betamap[i]]
            alpha = para[alphamap[i]]
            eexpr = beta * tempS[j] * IrN[i] / NN[i] * dt2  # new exposed due to reported cases
            eexpu = mu * beta * tempS[j] * IuN[i] / NN[i] * dt2  # new exposed due to unreported cases
            einfr = alpha * tempE[j] / Z * dt2  # new reported cases
            einfu = (1-alpha) * tempE[j] / Z * dt2  # new unreported cases
            erecr = tempIr[j] / D * dt2  # new recovery of reported cases
            erecu = tempIu[j] / D * dt2  # new recovery of unreported cases

            ERenter = theta * dt2 * C[j] / NN[i] * RentN[i]  # incoming r
            erleft = theta * dt2 * tempR[j] / (NN[i] - IrN[i]) * popleft[i]  # outgoing r
            eeenter = theta * dt2 * C[j] / NN[i] * EentN[i]  # incoming e
            eeleft = theta * dt2 * tempE[j] / (NN[i] - IrN[i]) * popleft[i]  # outgoing e
            eiuenter = theta * dt2 * C[j] / NN[i] * IuentN[i]  # incoming iu
            eiuleft = theta * dt2 * tempIu[j] / (NN[i] - IrN[i]) * popleft[i]  # outgoing iu

            expr = min(np.random.poisson(eexpr), (tempS[j]*dt2))
            expu = min(np.random.poisson(eexpu), (tempS[j]*dt2))
            infr = min(np.random.poisson(einfr), (tempE[j]*dt2))
            infu = min(np.random.poisson(einfu), (tempE[j]*dt2))
            recr = min(np.random.poisson(erecr), (tempIr[j]*dt2))
            recu = min(np.random.poisson(erecu), (tempIu[j]*dt2))

            Renter = np.random.poisson(ERenter)
            rleft = min(np.random.poisson(erleft), (tempR[j]*dt2))
            eenter = np.random.poisson(eeenter)
            eleft = min(np.random.poisson(eeleft), (tempE[j]*dt2))
            iuenter = np.random.poisson(eiuenter)
            iuleft = min(np.random.poisson(eiuleft), (tempIu[j]*dt2))

            newR[j] = max((newR[j] + recr + recu + Renter - rleft), 0)
            newE[j] = max((newE[j] + expr + expu - infr - infu + eenter - eleft), 0)
            newIr[j] = max((newIr[j] + infr - recr), 0)
            newIu[j] = max((newIu[j] + infu - recu + iuenter - iuleft), 0)
            dailyIr[j] = max((dailyIr[j] + infr), 0)
            dailyIu[j] = max((dailyIu[j] + infu), 0)
            newS[j] = max((C[j] - newE[j] - newIr[j] - newIu[j] - newR[j]), 0)
    return newS[:, 0], newE[:, 0], newIr[:, 0], newIu[:, 0], dailyIr[:, 0], dailyIu[:, 0]


def make_matrix(num_rows, num_cols, dtype):
    return np.zeros((num_rows, num_cols), dtype=dtype)


if __name__ == '__main__':
    nl = np.zeros((60232, 1))
    part = np.zeros(3143, dtype=np.int)

    C = np.zeros((60232, 89))
    Cave = copy.deepcopy(C)

    S = np.zeros((60232, 100))
    E = copy.deepcopy(S)
    Ir = copy.deepcopy(S)
    Iu = copy.deepcopy(S)

    para = np.zeros((435, 100))
    betamap = np.zeros((3141, 1))
    alphamaps = copy.deepcopy(betamap)

    t = 1
    k = 1

    model_eakf(nl, part, C[:, t], Cave[:, t], S[:, k], E[:, k], Ir[:, k], Iu[:, k], para[:, k], betamap, alphamaps)
