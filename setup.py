from setuptools import setup

from Cython.Build import cythonize
import numpy

setup(
    name='model_eakf',
    ext_modules=cythonize("model_eakf.pyx", annotate=True),
    include_dirs=[numpy.get_include()],
    zip_safe=False,
)

setup(
    name='adjustmobility',
    ext_modules=cythonize("adjustmobility.pyx", annotate=True),
    include_dirs=[numpy.get_include()],
    zip_safe=False,
)
