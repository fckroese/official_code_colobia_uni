import logging

import numpy as np


logger = logging.getLogger(__name__)


def relative_movement(inter_county_movement_shifted, day_beginning):
    inter_county_movement_relative = inter_county_movement_shifted
    for time in range(day_beginning, inter_county_movement_shifted.shape[1]):
        inter_county_movement_relative[:, time] /= inter_county_movement_relative[:, time - 1]
        inter_county_movement_relative[np.isnan(inter_county_movement_relative[:, time]), time] = 0
        inter_county_movement_relative[np.isinf(inter_county_movement_relative[:, time]), time] = 0
        inter_county_movement_relative[:, time] = np.minimum(inter_county_movement_relative[:, time], 1)
    inter_county_movement_relative[:, range(24)] = 1
    return inter_county_movement_relative


def initialise(neighbour_list, partition, commuters, num_ensembles, incidence):
    logger.info('initialising')
    num_locations = len(partition) - 1
    num_sub_pop = len(neighbour_list)
    susceptible = commuters.reshape(-1, 1) * np.ones((1, num_ensembles))  # Hoezo zijn de susceptibles alleen de mensen die commuten!?
    exposed = np.zeros((num_sub_pop, num_ensembles))
    infected_recorded = np.zeros((num_sub_pop, num_ensembles))
    infected_not_recorded = np.zeros((num_sub_pop, num_ensembles))

    num_times = np.shape(incidence)[1]
    seed_infected_not_recorded = np.zeros((num_locations, num_times))

    reported_cases = np.zeros((num_locations, 2))
    reported_cases[:, 0] = np.sum(incidence, axis=1)  # total incidence
    reported_cases[:, 1] = np.array(list(range(num_locations)))
    reported_cases = reported_cases[np.argsort(reported_cases[:, 0])][::-1]
    num_cases_greater_than_zero = reported_cases[:, 0] > 0
    seed_locations = reported_cases[num_cases_greater_than_zero, 1]

    # This loop is done to calculate c
    for location in range(np.size(seed_locations)):
        seedloc = int(seed_locations[location])
        temp = incidence[seedloc, :]
        index = np.where(temp > 0)[0]  # this is to find the index where infections>0
        if ~(index.size == 0):
            t0 = index[0]  # first reporting
            average = np.sum(temp[range(t0, min(t0 + 5, num_times))])  # hier wordt c berekent, als de som van de eerste 5 dagen van de infectie
            seed_infected_not_recorded[seedloc, max(0, t0-8)] = average  # vanaf T_0 verandert seed_commuters in c dit staat in de initialisation van de paper
    return susceptible, exposed, infected_recorded, infected_not_recorded, seed_infected_not_recorded


def initialise_parameters_EAKF(daily_incidence, num_ensembles, prior_parameter_settings, population_density):
    logger.info('initialise parameters')
    # Prior ranges of the variables
    z_low = 2  # latency period
    z_high = 5
    d_low = 2  # infectious_period
    d_high = 5
    mu_low = 0.1  # relative_transmissibility
    mu_high = 1.0
    theta_low = 0.01  # movement factor
    theta_high = 0.3
    alpha_low = 0.03  # reporting rate
    alpha_high = 0.25
    beta_low = 0.01  # transmission rate
    beta_high = 2.5

    # The selected area's
    num_locations = np.shape(daily_incidence)[0]
    ranks = np.zeros((num_locations, 2))
    ranks[:, 0] = np.array(list(range(1, num_locations + 1)))
    ranks[:, 1] = np.sum(daily_incidence, axis=1)
    ranks = ranks[np.argsort(ranks[:, 1])][::-1]
    # checked
    # Define which area's have a great enough case count and determine their
    # alpha:
    counties_with_more_than_400 = ranks[:, 1] >= 400
    selected = ranks[counties_with_more_than_400, 0]
    alphamap = np.ones((num_locations, 1)) * 4
    betamap = np.zeros((num_locations))
    betamap[counties_with_more_than_400] = 5 + np.array(list(range(1, np.shape(selected)[0]+1)))
    pd = np.log10(population_density)
    pd_median = np.median(pd)
    # determinining what beta belongs to what county
    factors = np.ones((np.shape(selected)[0] + 16))
    scale = 0.8
    for i in range(np.shape(selected)[0]):
        loc = int(selected[i] - 1)
        factors[i] = pd[loc] / pd_median * scale
    # now for counties with infections smaller than 400 to cut of the group at
    # 400 infections seems kind of arbitrary. No real reason for why this
    # should be at 400 maybe give a display of what the infections are per
    # county and show what the top 1% of counties have as case number or give
    # every major city a
    notselected = ranks[~counties_with_more_than_400, 0]
    notselected = np.sort(notselected).astype(int)
    total_cases = np.sum(daily_incidence, axis=1)
    notselected_total = total_cases[notselected - 1]
    totalrank = np.zeros((np.shape(notselected)[0], 2))
    totalrank[:, 0] = notselected
    totalrank[:, 1] = notselected_total
    totalrank = totalrank[np.argsort(totalrank[:, 1])][::-1]
    pdrank = np.ones((np.shape(notselected)[0], 2))
    pdrank[:, 0] = notselected
    pdrank[:, 1] = pd[notselected - 1]
    pdrank = pdrank[np.argsort(pdrank[:, 1])][::-1]
    # in pdrank are all counties that have not been selected, they matrix is
    # sorted on the basis of the number of infections.

    for iteration in range(np.shape(notselected)[0]):
        limited = notselected[iteration]
        caserankid = int(np.where(totalrank[:, 0] == limited)[0] + 1)
        caserankid = np.ceil(caserankid / (int(np.shape(notselected)[0] / 4))).astype(int)
        # caserankid gives the county a ranking on the basis of its number of coronapatients
        pdrankid = np.where(pdrank[:, 0] == limited)[0]
        pdrankid = np.ceil(pdrankid / (np.shape(notselected)[0] / 4)).astype(int)
        # the county also gets a rating on the basis of it's population
        # density the pdrank is a sorted matrix of population densities.
        betamap[limited - 1] = 5 + np.shape(selected)[0] + (caserankid - 1) * 4 + pdrankid
        # betamap is a vector that stores the combined values of caser rankind
        # and population density ranking
        low_range = int((pdrankid - 1) * np.floor(np.shape(notselected)[0] / 4))
        high_range = int(pdrankid * np.floor(np.shape(notselected)[0] / 4))
        # population densities die by de county in de buurt zitten worden ook
        # meegenomen bij de berekening van de average population density
        pdave = pdrank[range(low_range, high_range), 1]
        pdave = np.mean(pdave)
        factors[np.shape(selected)[0] - 1 + (caserankid - 1) * 4 + pdrankid] = (pdave / pd_median) * 0.8

    import pdb;pdb.set_trace()
    parameters_min = np.array([z_low, d_low, mu_low, theta_low, alpha_low] + (np.shape(selected)[0] + 16) * [beta_low])
    parameters_max = np.array([z_high, d_high, mu_high, theta_high, alpha_high] + (np.shape(selected)[0] + 16) * [beta_high])
    parameters = np.zeros((np.shape(parameters_min)[0], num_ensembles))

    parameters[0, range(num_ensembles)] = prior_parameter_settings[2, np.ceil(np.random.uniform(0, 1, num_ensembles) * num_ensembles).astype(int) - 1]
    parameters[1, range(num_ensembles)] = prior_parameter_settings[3, np.ceil(np.random.uniform(0, 1, num_ensembles) * num_ensembles).astype(int) - 1]
    parameters[2, range(num_ensembles)] = prior_parameter_settings[1, np.ceil(np.random.uniform(0, 1, num_ensembles) * num_ensembles).astype(int) - 1]
    parameters[3, range(num_ensembles)] = prior_parameter_settings[5, np.ceil(np.random.uniform(0, 1, num_ensembles) * num_ensembles).astype(int) - 1]
    parameters[4, range(num_ensembles)] = prior_parameter_settings[4, np.ceil(np.random.uniform(0, 1, num_ensembles) * num_ensembles).astype(int) - 1]

    for i in range(5, np.shape(parameters_min)[0]):
        parameters[i, :] = prior_parameter_settings[0, np.ceil(np.random.uniform(0, 1, num_ensembles) * num_ensembles).astype(int) - 1] * factors[i-5]
    return parameters, parameters_max, parameters_min, betamap, alphamap


def checkbounds_parameters(parameters, parameters_max, parameters_min):
    logger.info('checking bounds')
    for iteration in range(np.shape(parameters)[0]):
        if iteration > 3 :
            parameters[iteration, parameters[iteration, :] < parameters_min[iteration]] = [parameters_min[iteration]] * (1 + 0.1 * np.random.uniform(0, 1, np.sum(parameters[iteration, :] < parameters_min[iteration])))
            parameters[iteration, parameters[iteration, :] > parameters_max[iteration]] = [parameters_max[iteration]] * (1 + 0.1 * np.random.uniform(0, 1, np.sum(parameters[iteration, :] > parameters_max[iteration])))
    return parameters


def checkbounds_virus_model(susceptible, exposed, infected_recorded, infected_not_recorded):
    for k in range(susceptible.shape[1]):
        susceptible[susceptible[:, k] < 0, k] = 0
        exposed[exposed[:, k] < 0, k] = 0
        infected_recorded[infected_recorded[:, k] < 0, k] = 0
        infected_not_recorded[infected_not_recorded[:, k] < 0, k] = 0
    return susceptible, exposed, infected_recorded, infected_not_recorded


def adjustmobility(susceptible, exposed, infected_recorded, infected_not_recorded, neighbour_list, partition, inter_county_movement_relative, time):
    num_locations = np.shape(inter_county_movement_relative)[0]
    for location_a in range(num_locations):
        for location_b in range((partition[location_a] + 1), partition[location_a + 1] - 1):
            if time <= np.shape(inter_county_movement_relative)[1]:
                susceptible[partition[location_a]] = susceptible[location_a] + ((1 - inter_county_movement_relative[neighbour_list[location_b], time]) * susceptible[location_b])
                # this is very illogical the number of susceptible in a
                # location depends on the number of susceptible in that
                # location plus the relative difference in
                # inter_county_movement relative??????
                # this could be because otherwise the number of exposed changes
                # The difference in the number of susceptible in locatin b is
                # added to the number of susceptible in location a??
                exposed[partition[location_a]] = exposed[location_a] + ((1 - inter_county_movement_relative[neighbour_list[location_b], time]) * exposed[location_b])
                infected_recorded[partition[location_a]] = infected_recorded[location_a] + ((1 - inter_county_movement_relative[neighbour_list[location_b], time]) * infected_recorded[location_b])
                infected_not_recorded[partition[location_a]] = infected_recorded[location_a] + ((1 - inter_county_movement_relative[neighbour_list[location_b], time]) * infected_not_recorded[location_b])
                #  The numbers are adjusted here in accordance with changes in
                #  movement.
                susceptible[location_b] = inter_county_movement_relative[neighbour_list[location_b], time] * susceptible[location_b]
                exposed[location_b] = inter_county_movement_relative[neighbour_list[location_b], time] * exposed[location_b]
                infected_recorded[location_b] = inter_county_movement_relative[neighbour_list[location_b], time] * infected_recorded[location_b]
                infected_not_recorded[location_b] = inter_county_movement_relative[neighbour_list[location_b], time] * infected_not_recorded[location_b]
    return susceptible, exposed, infected_recorded, infected_not_recorded


def seeding(susceptible, exposed, infected_recorded, infected_not_recorded, neighbour_list, partition, commuters, seed_commuters, time):
    num_locations = np.shape(seed_commuters)[0]
    # Seed_commuters is the adjusted incidence rate based on c calculated in a previous
    # function
    num_ensembles = np.shape(susceptible)[1]
    for locations in range(num_locations):
        seedloc = locations
        seed_exposed = (np.random.uniform(0, 1, num_ensembles) * 20 * seed_commuters[locations, time])
        # exposed worden getrokken uit een uniforme verderling tussen de 0 en
        # 20c
        seed_infected_not_recorded = (np.random.uniform(0, 1, num_ensembles) * 18 * seed_commuters[locations, time])
        # infected_not_recorded worden ook getrokken uit een uniforme
        # verdeling maar dan range 0, 18c
        if seed_commuters[locations, time] > 0:
            population = np.sum(commuters[range(partition[seedloc], partition[seedloc + 1] - 1)])
            # population wordt bepaald aan de hand van het totale aantal mensen
            # in de commuters flow, the total population that partisipates in
            # this flow is the population
            for iteration in range(partition[seedloc], partition[seedloc + 1] - 1):
                exposed[iteration, :] = (seed_exposed * commuters[iteration]) / population
                # exposed wordt bepaald voor elke commuters_flow aan de hand
                # van de seed van het aantal infected keer de fractie van de
                # mensen die met die flow meedoet. Seed exposed zijn het
                # geschatte aantal mensen dat exposed zijn
                infected_not_recorded[iteration, :] = (seed_infected_not_recorded) * commuters[iteration] / population
                susceptible[iteration, :] = susceptible[iteration, :] - exposed[iteration, :] - infected_recorded[iteration, :] - infected_not_recorded[iteration, :]
    return susceptible, exposed, infected_recorded, infected_not_recorded


def random_delay_time(average, shape_param, size):
    delay = np.ceil(np.random.gamma(shape_param, average/shape_param, size))
    delay = delay.astype(int)
    return delay


def temp_function(temp_observations, time_1, k, num_locations, partition, daily_infected_recorded_temp, reporting_delay, num_times):
    for location_a in range(num_locations):
        for location_b in range(partition[location_a], partition[location_a + 1]):
            incidence = int(np.round(daily_infected_recorded_temp[location_b]))
            if incidence > 0:
                reporting_delay_1 = np.random.choice(reporting_delay, incidence)
                for high in range(incidence):
                    if (time_1 + reporting_delay_1[high]) < num_times:
                        # if the time + reporting delay is smaller than
                        # the maximum size of the dataset the
                        # observation for that time period will be
                        # added one this is done for every location 16
                        # days in the future and every ensemble. heel
                        temp_observations[location_a, k, time_1 + reporting_delay_1[high]] += 1
    return temp_observations


def temp_function_death(death_temp, time_1, k, num_locations, partition, daily_infected_recorded_temp, daily_infected_not_recorded_temp, delay_of_death, death_rate, num_times):
    for location_a in range(num_locations):
        for location_b in range(partition[location_a], partition[location_a + 1]):
            incidence = int(np.round(daily_infected_recorded_temp[location_b] + daily_infected_not_recorded_temp[location_b] * death_rate[location_a]))
            if incidence > 0:
                delay_of_death_1 = np.random.choice(delay_of_death, incidence)
                for high in range(incidence):
                    if (time_1 + delay_of_death_1[high]) < num_times:
                        # hier geldt ook if the time + death delay is
                        # smaller than the maximum size of the dataset
                        # a death is added tot that date and that
                        # location this is again done for every
                        # ensemble.
                        death_temp[location_a, k, time_1 + delay_of_death_1[high]] += 1
    return death_temp


def the_first_loop_with_death(lambda_to_avoid_ensemble_collapse, part_ensemble, parameters, num_times, alphamap, betamap, obs_error_variance, roling_average, exposed, infected_recorded, infected_not_recorded, daily_infected_not_recorded_prior, daily_infected_recorded_prior, partition, neighbour_list, location_a, time_1, parameters_std, num_ensembles):
    # in order to use the ensemble adjustment Kalman filter certain
    # variances and means need to be specified that is done before the loop
    # First the variances are specified :
    observations_variance = obs_error_variance[location_a, time_1]
    prior_variance = np.var(part_ensemble[location_a, :])
    post_variance = prior_variance * observations_variance / (prior_variance + observations_variance)  # deze formule is afgeleid neem ik aan checken in theretisch kader 
    # If degerenate, so make sure the variance is not degenerate
    if prior_variance == 0:
        post_variance = 0.001
        prior_variance = 0.001
    # next the prior mean and the posterior mean that has a predetermined
    # mean as follows from theory
    prior_mean = np.mean(part_ensemble[location_a, :])
    post_mean = post_variance * (prior_mean / prior_variance + roling_average[location_a, time_1] / observations_variance)
    # compute alpha and adjust distribution to form to posterior moments
    alpha = np.sqrt(observations_variance / (observations_variance + prior_variance))
    delta_observed_mean = post_mean + alpha * (part_ensemble[location_a, :] - prior_mean) - part_ensemble[location_a, :]
    # loop over each state variable (connected to location_a l) adjust related
    # metapopulation
    neighbour_live_l = range(partition[location_a], partition[location_a + 1])
    neighbour_work_l = np.where(neighbour_list == location_a)[0]
    neighbours_union_1 = np.unique(np.append(neighbour_live_l, neighbour_work_l))
    for neighbours in range(np.shape(neighbours_union_1)[0]):
        neighbour = neighbours_union_1[neighbours]
        # exposed group
        temp_exposed = exposed[neighbour, :]
        covariance_a = np.cov(temp_exposed, part_ensemble[location_a, :])
        rr = covariance_a[1, 0] / prior_variance
        delta_x = rr * delta_observed_mean
        exposed[neighbour, :] += delta_x

        # infected_recorded
        temp_infected_recorded = infected_recorded[neighbour, :]
        covariance_a = np.cov(temp_infected_recorded, part_ensemble[location_a, :])
        rr = covariance_a[1, 0] / prior_variance
        delta_x = rr * delta_observed_mean
        infected_recorded[neighbour, :] += delta_x
        # infected_not_recorded
        temp_infected_not_recorded = infected_not_recorded[neighbour, :]
        covariance_a = np.cov(temp_infected_not_recorded, part_ensemble[location_a, :])
        rr = covariance_a[1, 0] / prior_variance
        delta_x = rr * delta_observed_mean
        infected_not_recorded[neighbour, :] += delta_x
        # Daily infected_not_recorded
        temp_infected_recorded = daily_infected_recorded_prior[neighbour, :]
        A = np.cov(temp_infected_recorded, part_ensemble[location_a, :])
        rr = A[1, 0] / prior_variance
        delta_x = rr * delta_observed_mean
        daily_infected_recorded_prior[neighbour, :] = np.max(daily_infected_recorded_prior[neighbour, :] + delta_x, 0)
        # Daily infected_not_recorded
        temp_infected_not_recorded = daily_infected_not_recorded_prior[neighbour, :]
        A = np.cov(temp_infected_not_recorded, part_ensemble[location_a, :])
        rr = A[1, 0] / prior_variance
        delta_x = rr * delta_observed_mean
        daily_infected_not_recorded_prior[neighbour, :] = np.max(daily_infected_not_recorded_prior[neighbour, :] + delta_x, 0)
    # adjust alpha before running out of observations of case and death
    if (time_1 + 9 < num_times):
        # i think this is done because delay of death is 9 days
        temporary = parameters[alphamap[location_a], :]
        A = np.cov(temporary, part_ensemble[location_a, :])
        rr = A[1, 0] / prior_variance
        delta_x = rr * delta_observed_mean
        parameters[alphamap[location_a], :] = parameters[alphamap[location_a], :] + delta_x
        # inflation why inflate this parameter ?
        if np.std(parameters[alphamap[location_a], :], ddof=1) < parameters_std[alphamap[location_a]]:
            parameters[alphamap[location_a], :] = np.mean(parameters[alphamap[location_a], :]) * np.ones(num_ensembles) + lambda_to_avoid_ensemble_collapse * np.mean(parameters[alphamap[location_a], :]) * np.ones(num_ensembles)
    # adjust beta
    temporary = parameters[betamap[location_a], :]
    A = np.cov(temporary, part_ensemble[location_a, :])
    rr = A[1, 0] / prior_variance
    delta_x = rr * delta_observed_mean
    parameters[betamap[location_a], :] = parameters[betamap[location_a], :] + delta_x
    # inflation why?
    if np.std(parameters[betamap[location_a], :], ddof=1) < parameters_std[betamap[location_a]]:
        parameters[betamap[location_a], :] = np.mean(parameters[betamap[location_a], :]) * np.ones(num_ensembles) + lambda_to_avoid_ensemble_collapse * np.mean(parameters[betamap[location_a], :]) * np.ones(num_ensembles)
    return part_ensemble, parameters, exposed, infected_recorded, infected_not_recorded, daily_infected_recorded_prior, daily_infected_not_recorded_prior


def the_second_loop_with_cases(num_ensembles, lambda_to_avoid_ensemble_collapse, part_ensemble, parameters, num_times, alphamap, betamap, obs_error_variance, roling_average, exposed, infected_recorded, infected_not_recorded, daily_infected_not_recorded_prior, daily_infected_recorded_prior, partition, neighbour_list, location_a, time_1, parameters_std):
    # in order to use the ensemble adjustment Kalman filter certain
    # variances and means need to be specified that is done before the loop
    # First the variances are specified :
    observations_variance = obs_error_variance[location_a, time_1]
    prior_variance = np.var(part_ensemble[location_a, :])
    post_variance = prior_variance * observations_variance / (prior_variance + observations_variance)  # deze formule is afgeleid neem ik aan checken in theretisch kader 
    # If degerenate, so make sure the variance is not degenerate
    if prior_variance == 0:
        post_variance = 0.001
        prior_variance = 0.001
    # next the prior mean and the posterior mean that has a predetermined
    # mean as follows from theory
    prior_mean = np.mean(part_ensemble[location_a, :])
    post_mean = post_variance * (prior_mean / prior_variance + roling_average[location_a, time_1] / observations_variance)
    # compute alpha and adjust distribution to form to posterior moments
    alpha = np.sqrt(observations_variance / (observations_variance + prior_variance))
    delta_observed_mean = post_mean + alpha * (part_ensemble[location_a, :] - prior_mean) - part_ensemble[location_a, :]
    # loop over each state variable (connected to location_a l) adjust related
    # metapopulation
    neighbour_live_l = range(partition[location_a], partition[location_a + 1])
    neighbour_work_l = np.where(neighbour_list == location_a)[0]
    neighbours_union_1 = np.unique(np.append(neighbour_live_l, neighbour_work_l))
    for neighbours in range(np.shape(neighbours_union_1)[0]):
        neighbour = neighbours_union_1[neighbours]
        # exposed group
        temp_exposed = exposed[neighbour, :]
        covariance_a = np.cov(temp_exposed, part_ensemble[location_a, :])
        rr = covariance_a[1, 0] / prior_variance
        delta_x = rr * delta_observed_mean
        exposed[neighbour, :] += delta_x
        # infected_recorded
        temp_infected_recorded = infected_recorded[neighbour, :]
        covariance_a = np.cov(temp_infected_recorded, part_ensemble[location_a, :])
        rr = covariance_a[1, 0] / prior_variance
        delta_x = rr * delta_observed_mean
        infected_recorded[neighbour, :] += delta_x
        # infected_not_recorded
        temp_infected_not_recorded = infected_not_recorded[neighbour, :]
        covariance_a = np.cov(temp_infected_not_recorded, part_ensemble[location_a, :])
        rr = covariance_a[1, 0] / prior_variance
        delta_x = rr * delta_observed_mean
        infected_not_recorded[neighbour, :] += delta_x
        # Daily infected_not_recorded
        temp_infected_recorded = daily_infected_recorded_prior[neighbour, :]
        A = np.cov(temp_infected_recorded, part_ensemble[location_a, :])
        rr = A[1, 0] / prior_variance
        delta_x = rr * delta_observed_mean
        daily_infected_recorded_prior[neighbour, :] = np.max(daily_infected_recorded_prior[neighbour, :] + delta_x, 0)
        # Daily infected_not_recorded
        temp_infected_not_recorded = daily_infected_not_recorded_prior[neighbour, :]
        A = np.cov(temp_infected_not_recorded, part_ensemble[location_a, :])
        rr = A[1, 0] / prior_variance
        delta_x = rr * delta_observed_mean
        daily_infected_not_recorded_prior[neighbour, :] = np.max(daily_infected_not_recorded_prior[neighbour, :] + delta_x, 0)
    # adjust alpha before running out of observations of case and death
    if (time_1 + 9 < num_times):
        # i think this is done because delay of death is 9 days
        temporary = parameters[alphamap[location_a], :]
        A = np.cov(temporary, part_ensemble[location_a, :])
        rr = A[1, 0] / prior_variance
        delta_x = rr * delta_observed_mean
        parameters[alphamap[location_a], :] = parameters[alphamap[location_a], :] + delta_x
        # inflation why inflate this parameter ?
        if np.std(parameters[alphamap[location_a], :], ddof=1) < parameters_std[alphamap[location_a]]:
            parameters[alphamap[location_a], :] = np.mean(parameters[alphamap[location_a], :]) * np.ones(num_ensembles) + lambda_to_avoid_ensemble_collapse * np.mean(parameters[alphamap[location_a], :]) * np.ones(num_ensembles)
    # adjust beta
    temporary = parameters[betamap[location_a], :]
    A = np.cov(temporary, part_ensemble[location_a, :])
    rr = A[1, 0] / prior_variance
    delta_x = rr * delta_observed_mean
    parameters[betamap[location_a], :] = parameters[betamap[location_a], :] + delta_x
    # inflation why?
    if np.std(parameters[betamap[location_a], :], ddof=1) < parameters_std[betamap[location_a]]:
        parameters[betamap[location_a], :] = np.mean(parameters[betamap[location_a], :]) * np.ones(num_ensembles) + lambda_to_avoid_ensemble_collapse * np.mean(parameters[betamap[location_a], :]) * np.ones(num_ensembles)
    return part_ensemble, parameters, exposed, infected_recorded, infected_not_recorded, daily_infected_recorded_prior, daily_infected_not_recorded_prior
