cimport numpy as np

ctypedef np.int_t DTYPE_INT
ctypedef np.float_t DTYPE_FLOAT


cpdef adjustmobility(
        np.ndarray[DTYPE_FLOAT, ndim=1] susceptible,
        np.ndarray[DTYPE_FLOAT, ndim=1] exposed,
        np.ndarray[DTYPE_FLOAT, ndim=1] infected_recorded,
        np.ndarray[DTYPE_FLOAT, ndim=1] infected_not_recorded,
        np.ndarray[DTYPE_INT, ndim=1] neighbour_list,
        np.ndarray[DTYPE_INT, ndim=1] partition,
        np.ndarray[DTYPE_FLOAT, ndim=2] inter_county_movement_relative,
        int time):

    cdef unsigned int num_locations = inter_county_movement_relative.shape[0]
    cdef unsigned int location_a, location_b
    for location_a in range(num_locations):
        for location_b in range((partition[location_a] + 1), partition[location_a + 1] - 1):
            if time <= inter_county_movement_relative.shape[1]:
                susceptible[partition[location_a]] = susceptible[location_a] + ((1 - inter_county_movement_relative[neighbour_list[location_b], time]) * susceptible[location_b])
                # this is very illogical the number of susceptible in a
                # location depends on the number of susceptible in that
                # location plus the relative difference in
                # inter_county_movement relative??????
                # this could be because otherwise the number of exposed changes
                # The difference in the number of susceptible in locatin b is
                # added to the number of susceptible in location a??
                exposed[partition[location_a]] = exposed[location_a] + ((1 - inter_county_movement_relative[neighbour_list[location_b], time]) * exposed[location_b])
                infected_recorded[partition[location_a]] = infected_recorded[location_a] + ((1 - inter_county_movement_relative[neighbour_list[location_b], time]) * infected_recorded[location_b])
                infected_not_recorded[partition[location_a]] = infected_recorded[location_a] + ((1 - inter_county_movement_relative[neighbour_list[location_b], time]) * infected_not_recorded[location_b])
                #  The numbers are adjusted here in accordance with changes in
                #  movement.
                susceptible[location_b] = inter_county_movement_relative[neighbour_list[location_b], time] * susceptible[location_b]
                exposed[location_b] = inter_county_movement_relative[neighbour_list[location_b], time] * exposed[location_b]
                infected_recorded[location_b] = inter_county_movement_relative[neighbour_list[location_b], time] * infected_recorded[location_b]
                infected_not_recorded[location_b] = inter_county_movement_relative[neighbour_list[location_b], time] * infected_not_recorded[location_b]
