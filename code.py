import logging
import time as timer

from scipy.io import loadmat
import numpy as np

from model_eakf import model_eakf
from adjustmobility import adjustmobility
import funcs


logger = logging.getLogger(__name__)


def setup_logger():
    for _logger in [logger, funcs.logger]:
        _logger.setLevel(logging.DEBUG)
        if not _logger.handlers:
            ch = logging.StreamHandler()
            ch.setLevel(logging.DEBUG)

            formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
            ch.setFormatter(formatter)

            _logger.addHandler(ch)


if __name__ == '__main__':
    setup_logger()
    logger.info('start the clock')

    # #######################3
    # values
    # mode: production or testing
    # #######################3
    start = timer.time()
    mode = 'PRODUCTION'
    T = 1  # vanaf approximately 21 februari 2020 tot 3 mei 2020
    num_ensembles = 100

    # Import data from the datasets for commutedata, populationdata,
    # dailyincidencedata, dailydeaths
    logger.info('Running in "{}" mode'.format(mode))
    logger.info('loading data')
    commute_data = loadmat('/Users/floriankroese/Documents/Scriptie/Colombia/code/data/{}/commutedata.mat'.format(mode))
    population_data = loadmat('/Users/floriankroese/Documents/Scriptie/Colombia/code/data/{}/population.mat'.format(mode))
    daily_incidence_data = loadmat('/Users/floriankroese/Documents/Scriptie/Colombia/code/data/{}/dailyincidence.mat'.format(mode))
    daily_death_data = loadmat('/Users/floriankroese/Documents/Scriptie/Colombia/code/data/{}/dailydeaths.mat'.format(mode))
    death_rate_per_county = loadmat('/Users/floriankroese/Documents/Scriptie/Colombia/code/data/{}/deathrate.mat'.format(mode))
    county_movement = loadmat('/Users/floriankroese/Documents/Scriptie/Colombia/code/data/{}/MI_inter.mat'.format(mode))
    population_density_data = loadmat('/Users/floriankroese/Documents/Scriptie/Colombia/code/data/{}/popd.mat'.format(mode))
    prior_parameter_settings_data = loadmat('/Users/floriankroese/Documents/Scriptie/Colombia/code/data/{}/parafit1.mat'.format(mode))

    # declare variables:
    population = population_data['population'].reshape(-1, )
    neighbour_list = commute_data['nl'].reshape(-1, ).astype(np.int) - 1
    partition = commute_data['part'].reshape(-1, ).astype(np.int) - 1
    commuters = commute_data['C'].reshape(-1, )
    average_commuters = commute_data['Cave'].reshape(-1, )
    daily_incidence = daily_incidence_data['dailyincidence']
    incidence_data_untill_may_3 = daily_incidence[:, range(T)]
    daily_deaths = daily_death_data['dailydeaths']
    death_rate = death_rate_per_county['deathrate'].reshape(-1, )
    inter_county_movement = county_movement['MI_inter']
    population_density = population_density_data['popd'].reshape(-1, )
    prior_parameter_settings = prior_parameter_settings_data['parafit']

    num_locations = np.shape(partition)[0] - 1
    number_sub_populations = np.shape(neighbour_list)[0]
    num_times = np.shape(daily_incidence)[1]
    incidence_roling_average = np.zeros(np.shape(daily_incidence))
    death_roling_average = np.zeros(np.shape(daily_deaths))
    death_temp = np.zeros((num_locations, num_ensembles, num_times))
    records_of_death = np.zeros(())
    oev_case = np.zeros(np.shape(daily_incidence))
    oev_death = np.zeros(np.shape(daily_deaths))
    commuters_adjusted = commuters.reshape(-1, 1) * np.ones((1, num_times))
    average_commuters_adjusted = average_commuters.reshape(-1, 1) * np.ones((1, num_times))

    # Here the reporting delay and the delay of death is defined.
    # Gamma distribution of the reporting delay

    # reporting delay:
    size = 10000
    av_reporting_delay = 9
    shape_param_rep_delay = 1.85
    reporting_delay = funcs.random_delay_time(average=av_reporting_delay, shape_param=shape_param_rep_delay, size=size)

    # Gamma distribution for the delay of death
    av_delay_of_death = 16
    shape_param_delay_of_death = 1.85
    delay_of_death = funcs.random_delay_time(average=av_delay_of_death, shape_param=shape_param_delay_of_death, size=size)

    # data smoothing by taking the 7 day roling average
    logger.info('data smoothing')
    for location in range(num_locations):
        for time in range(num_times):
            max_time = min(time + 4, num_times)
            min_time = max(1, time - 3)
            incidence_roling_average[location, time] = np.mean(daily_incidence[location, range(min_time, max_time)])
            death_roling_average[location, time] = np.mean(daily_deaths[location, range(min_time, max_time)])
    # The model is confined to data untill day 73 which is may 3rd 2020
    # This needs to be taken into account when calculating the observational error
    # variance. The observational error variance is calculated using the 7 day
    # moving average
    logger.info('death_roling_average')
    for location in range(num_locations):
        for time in range(num_times):
            min_time = max(1, time - 6)
            incidence_roling_ave_ave = np.mean(incidence_roling_average[location, range(min_time, time)])
            oev_case[location, time] = max(25, incidence_roling_ave_ave ** 2 / 100)
            death_roling_ave_ave = np.mean(death_roling_average[location, range(min_time, time)])
            oev_death[location, time] = max(25, death_roling_ave_ave ** 2 / 100)
    # why is the roling average calculated in two seperate ways?? the first time it
    # is calculated by taking the average of values 3 days in the future and 3 days
    # bays and for the observational error variance the data is taken 6 days back.
    # no consitency!!(COMMENT!!)
    # maybe say something of the way in which the obs_variance is calculated

    # now the inter county movement is adjusted the dataset contains the numbers of
    # the counties in the first column that's why the dataset is taken from the
    # second column
    # the movement is adjusted from day 25, 16 march therefore t=25: end of data
    # the second and third line are added to make sure no variables are equal to
    # infinity or nan. The data contains absolyte values of mobility. The model
    # wants relative values that indicate an increase or decrease in the mobility
    # with respect to the day beforel.
    # The fourth line is to make sure that relative inter county movement can not be higher than 1
    # a lot of observations are thrown away, because all counties that have a zero
    # in them are put to zero in their entirety!! On top of that the maximum
    # relative county_movement is 1 (this might be due to the role this variable
    # playes in the adjustement of the number of commuters)is this a good idea??(COMMENT!!)

    inter_county_movement_shifted = inter_county_movement[:, range(1, np.shape(inter_county_movement)[1])]
    day_beginning = 24
    inter_county_movement_relative = funcs.relative_movement(inter_county_movement_shifted, day_beginning=day_beginning)
    # as C denotes the number of people who commute between the different
    # partitions this needs to be adjusted aswell from date 16 march onwards
    # the number of commuters are adjusted in accordance with the inter_county
    # movement the number of commuters is calculated by summing up the number of
    # people who move from within partition back to itself and from neighbouring
    # paritions to the partition. Keep in mind that the inter_county_movement is
    # relative so if inter_county_movement_relative is one the number of
    # commuters remain unchainched. On top of that some commute flows were set
    # to zero and those flows have thus been deleted

    start = 25
    for time in range(start, num_times):
        logger.info('big old loop: {}/{}'.format(time, (num_times-start)))
        commuters_adjusted[:, time] = commuters_adjusted[:, time - 1]
        average_commuters_adjusted[:, time] = average_commuters_adjusted[:, time - 1]
        for location_a in range(num_locations):
            for location_b in range(partition[location_a] + 1, partition[location_a + 1]):
                if time <= np.shape(inter_county_movement_relative)[1] - 1:
                    # this is split in two parts first from a to b than from b to
                    # a, because commuting flows could be different.
                    commuters_adjusted[partition[location_a], time] = commuters_adjusted[partition[location_a], time] + \
                        ((1 - inter_county_movement_relative[neighbour_list[location_b], time]) * commuters_adjusted[location_b, time])
                    # the number of commuters is the number of commuters intern in
                    # the partition plus the number of people who go to the
                    # partition from other partions
                    average_commuters_adjusted[partition[location_a], time] = average_commuters_adjusted[partition[location_a], time] + \
                        ((1 - inter_county_movement_relative[neighbour_list[location_b], time]) * average_commuters_adjusted[location_b, time])
                    # the average of the number of commuters ikks the average number
                    # of commuters internally plus the relative change. Is always
                    commuters_adjusted[location_b, time] = (inter_county_movement_relative[neighbour_list[location_b], time] * commuters_adjusted[location_b, time])
                    average_commuters_adjusted[location_b, time] = (inter_county_movement_relative[neighbour_list[location_b], time] * average_commuters_adjusted[location_b, time])
                    # i guess it has something to do with that the number of
                    # commuters remains the same, unclear code though

    [susceptible, exposed, infected_recorded, infected_not_recorded, seed_commuters] = funcs.initialise(
        neighbour_list=neighbour_list,
        partition=partition,
        commuters_adjusted=commuters_adjusted[:, 0],
        num_ensembles=num_ensembles,
        incidence=incidence_data_untill_may_3
    )

    [parameters, parameters_max, parameters_min, betamap, alphamap] = funcs.initialise_parameters_EAKF(
        daily_incidence=daily_incidence,
        num_ensembles=num_ensembles,
        prior_parameter_settings=prior_parameter_settings,
        population_density=population_density
    )
    alphamap = alphamap.astype(int)[:, 0]
    betamap = betamap.astype(int)
    paramax_ori = parameters_max
    paramin_ori = parameters_min
    # Inflate parameters, WHY?
    lambda_value = 2
    parameters = np.mean(parameters, axis=1).reshape(-1, 1) * np.ones((1, num_ensembles)) + lambda_value * (parameters - np.mean(parameters, axis=1).reshape(-1, 1) * np.ones((1,num_ensembles)))
    parameters = funcs.checkbounds_parameters(parameters, parameters_max, parameters_min)

    # Fix parameters Z, D and theta
    parameters[0, :] = prior_parameter_settings[2, range(num_ensembles)]
    parameters[1, :] = prior_parameter_settings[3, range(num_ensembles)]
    parameters[2, :] = prior_parameter_settings[1, range(num_ensembles)]
    parameters[3, :] = prior_parameter_settings[5, range(num_ensembles)]

    # now the ensemble adjustment kalman filter will be defined for this we need
    # the varianve of the observations and a host of other variables.
    parameters_std = np.std(parameters, axis=1, ddof=1)
    # Ensemble adjustment Kalman filter
    lambda_to_avoid_ensemble_collapse = 1.2
    num_parameters = np.shape(parameters)[0]
    parameters_posterior = np.zeros((num_parameters, num_ensembles, T))
    # Posterior parameters
    parameter_posterior = np.zeros((num_parameters, num_ensembles, T))
    # Posterior susceptability
    susceptible_posterior = np.zeros((num_locations, num_ensembles, T))
    num_sub_populations = np.shape(neighbour_list)[0]

    for time in range(T):
        logger.info('parameter update: {}/{}'.format(time, T))
        # Smoothing constraint for parameters
        # Wat houdt dit in?
        for iteration in range(num_parameters):
            if iteration > 3:
                parameters_max[iteration] = np.minimum(np.mean(parameters[iteration, :]) * 13/10, paramax_ori[iteration])
                parameters_min[iteration] = np.maximum(np.mean(parameters[iteration, :]) * 7/10, paramin_ori[iteration])
                # the change can be 30% at maximum that's why this loop is there, but again no real reason is given why 30% should be the maximum
        parameters[0, :] = prior_parameter_settings[2, range(num_ensembles)]
        parameters[1, :] = prior_parameter_settings[3, range(num_ensembles)]
        parameters[2, :] = prior_parameter_settings[1, range(num_ensembles)]
        parameters[3, :] = prior_parameter_settings[5, range(num_ensembles)]

        # seeding
        if time <= np.shape(seed_commuters)[1]:
            [susceptible, exposed, infected_recorded, infected_not_recorded] = funcs.seeding(
                susceptible=susceptible,
                exposed=exposed,
                infected_recorded=infected_recorded,
                infected_not_recorded=infected_not_recorded,
                neighbour_list=neighbour_list,
                partition=partition,
                commuters=commuters_adjusted[:, time],
                seed_commuters=seed_commuters,
                time=time
            )

        # recording model variables
        susceptible_temporary = susceptible
        exposed_temporary = exposed
        infected_recorded_temporary = infected_recorded
        infected_not_recorded_temporary = infected_not_recorded
        # Integrate forward one time-step
        daily_infected_recorded_prior = np.zeros((num_sub_populations, num_ensembles))
        daily_infected_not_recorded_prior = np.zeros((num_sub_populations, num_ensembles))
        for k in range(num_ensembles):
            logger.info('loop through the model_eakf.py: {},{}/{},{}'.format(time, k, T, num_ensembles))
            # adjust population according to change of inter - county movement
            _s = timer.time()

            # BEWARE: this function changes the input array IN-PLACE. Be
            # careful not to pass in copies of the data, because the changes
            # will be lost.

            adjustmobility(
                susceptible[:, k],
                exposed[:, k],
                infected_recorded[:, k],
                infected_not_recorded[:, k],
                neighbour_list,
                partition,
                inter_county_movement_relative,
                time
            )
            _e = timer.time()
            logger.info('elapsed: {}'.format(_e - _s))

            _s = timer.time()
            [susceptible[:, k], exposed[:, k], infected_recorded[:, k], infected_not_recorded[:, k], dailyIr_temp, dailyIu_temp] = model_eakf(
                neighbour_list,
                partition,
                commuters_adjusted[:, time],
                average_commuters_adjusted[:, time],
                susceptible[:, k],
                exposed[:, k],
                infected_recorded[:, k],
                infected_not_recorded[:, k],
                parameters[:, k],
                betamap,
                alphamap
            )
            daily_infected_recorded_prior[:, k] = dailyIr_temp
            daily_infected_not_recorded_prior[:, k] = dailyIu_temp

            _e = timer.time()
            logger.info('elapsed: {}'.format(_e - _s))

        #################################################################################################
        daily_infected_recorded_temp = np.ones(np.shape(susceptible)[0])  # uncomment this line
        daily_infected_not_recorded_temp = np.ones(np.shape(susceptible)[0])  # uncomment this line
        # mini forecast based on current model state
        # integrate forwarde for 16 days, prepare for observation
        t_projected = np.minimum(16, num_times - time)
        observations_temp_1 = np.zeros((num_locations, num_ensembles, num_times))

        death_temp_1 = death_temp
        time_1_end = time + t_projected
        for time_1 in range(time, time_1_end):
            for k in range(num_ensembles):  # run for each ensemble member seperately
                logger.info('loop through the model_eakf.py: {}/{}, {}/{}, {}/{}'.format(time_1, time_1_end, k, num_ensembles, time + 1, T))
                adjustmobility(
                    susceptible[:, k],
                    exposed[:, k],
                    infected_recorded[:, k],
                    infected_not_recorded[:, k],
                    neighbour_list,
                    partition,
                    inter_county_movement_relative,
                    time_1
                )
                [susceptible[:, k], exposed[:, k], infected_recorded[:, k], infected_not_recorded[:, k], daily_infected_recorded_temp, daily_infected_not_recorded_temp] = model_eakf(
                    neighbour_list,
                    partition,
                    commuters_adjusted[:, time],
                    average_commuters_adjusted[:, time],
                    susceptible[:, k],
                    exposed[:, k],
                    infected_recorded[:, k],
                    infected_not_recorded[:, k],
                    parameters[:, k],
                    betamap,
                    alphamap
                    )

                observations_temp_1 = funcs.temp_function(
                    temp_observations=observations_temp_1,
                    time_1=time_1,
                    k=k,
                    num_locations=num_locations,
                    partition=partition,
                    daily_infected_recorded_temp=daily_infected_recorded_temp,
                    reporting_delay=reporting_delay,
                    num_times=num_times
                )
                death_temp_1 = funcs.temp_function_death(
                    death_temp=death_temp_1,
                    time_1=time_1,
                    k=k,
                    num_locations=num_locations,
                    partition=partition,
                    daily_infected_recorded_temp=daily_infected_recorded_temp,
                    daily_infected_not_recorded_temp=daily_infected_not_recorded_temp,
                    delay_of_death=delay_of_death,
                    death_rate=death_rate,
                    num_times=num_times
                )

        #################################################################################################
        # update model using the prediction of the previous section
        time_1 = np.minimum(time + t_projected, num_times)
        death_ensemble = death_temp_1[:, :, time_1]  # this is number of deaths at t1, so the prior
        # loop through all local observations
        logger.info('first update using prediction')
        for location_a in range(num_locations):
            [death_ensemble, parameters, exposed, infected_recorded, infected_not_recorded, daily_infected_recorded_prior, daily_infected_not_recorded_prior] = funcs.the_first_loop_with_death(
                lambda_to_avoid_ensemble_collapse=lambda_to_avoid_ensemble_collapse,
                part_ensemble=death_ensemble,
                parameters=parameters,
                num_times=num_times,
                alphamap=alphamap,
                betamap=betamap,
                obs_error_variance=oev_death,
                roling_average=death_roling_average,
                exposed=exposed,
                infected_recorded=infected_recorded,
                infected_not_recorded=infected_not_recorded,
                daily_infected_not_recorded_prior=daily_infected_not_recorded_prior,
                daily_infected_recorded_prior=daily_infected_recorded_prior,
                partition=partition,
                neighbour_list=neighbour_list,
                location_a=location_a,
                time_1=time_1,
                parameters_std=parameters_std,
                num_ensembles=num_ensembles
            )

        #############################################################################################################
        # update model using 9 days ahead

        logger.info('update using 9 days ahead')
        time_1 = np.minimum(time + 9, num_times)
        observational_ensemble = observations_temp_1[:, :, time_1]
        for location_a in range(num_locations):
            # get the variance of the ensemble
            [observational_ensemble, parameters, exposed, infected_recorded, infected_not_recorded, daily_infected_recorded_prior, daily_infected_not_recorded_prior] = funcs.the_second_loop_with_cases(
                num_ensembles=num_ensembles,
                lambda_to_avoid_ensemble_collapse=lambda_to_avoid_ensemble_collapse,
                part_ensemble=observational_ensemble,
                parameters=parameters,
                num_times=num_times,
                alphamap=alphamap,
                betamap=betamap,
                obs_error_variance=oev_case,
                roling_average=death_roling_average,
                exposed=exposed,
                infected_recorded=infected_recorded,
                infected_not_recorded=infected_not_recorded,
                daily_infected_not_recorded_prior=daily_infected_not_recorded_prior,
                daily_infected_recorded_prior=daily_infected_recorded_prior,
                partition=partition,
                neighbour_list=neighbour_list,
                location_a=location_a,
                time_1=time_1,
                parameters_std=parameters_std
            )

        ##############################################################################################################
        parameters = funcs.checkbounds_parameters(
            parameters,
            parameters_max=parameters_max,
            parameters_min=parameters_min
        )
        # updtae infected_recorded na infected_not_recorded
        daily_infected_not_recorded_post = daily_infected_recorded_prior
        daily_infected_recorded_post = daily_infected_recorded_prior
        # fix alpha after running out of observations
        if (time + 9) >= num_times:
            parameters[4, :] = parameters_posterior[4, :, time - 1]
        # update observations
        logger.info('update observations')
        for k in range(num_ensembles):
            observations_temp_1 = funcs.temp_function(temp_observations=observations_temp_1, time_1=time_1,
                                                      k=k, num_locations=num_locations, partition=partition,
                                                      daily_infected_recorded_temp=daily_infected_recorded_temp,
                                                      reporting_delay=reporting_delay, num_times=num_times)
            death_temp_1 = funcs.temp_function_death(death_temp=death_temp_1, time_1=time_1, k=k,
                                                     num_locations=num_locations, partition=partition,
                                                     daily_infected_recorded_temp=daily_infected_recorded_temp,
                                                     daily_infected_not_recorded_temp=daily_infected_not_recorded_temp,
                                                     delay_of_death=delay_of_death, death_rate=death_rate,
                                                     num_times=num_times)
        [susceptible, exposed, infected_recorded, infected_not_recorded] = funcs.checkbounds_virus_model(
            susceptible,
            exposed,
            infected_recorded,
            infected_not_recorded
        )
        parameter_posterior[:, :, time] = parameters
        for location_a in range(num_locations):
            susceptible_posterior[location_a, :, time] = np.minimum(1, np.sum(susceptible[range(partition[location_a], partition[location_a + 1])]) / population[location_a])
        logger.info('first loop finito')

    print('The first loop took: ', duration_of_script)
       # Hier moeten de posterior parameters nog worden opgeslagen
